class SimpleCache {
  constructor() {
    this.store = {
      freewillies: [],
      rooms: {}
    };
  }

  get(key) {
    return this.store[key];
  }

  getIfPresent(key) {
    if (this.containsKey(key)) {
      return this.store[key];
    }
    return null;
  }

  put(key, value) {
    this.store[key] = value;
  }

  containsKey(key) {
    return this.store.hasOwnProperty(key);
  }

  /**
   * put user in cache without room
   * @param conId
   * @param idUser
   * @param apiToken
   * @param username
   */
  addUserToFreeWilly(conId, idUser, apiToken, username) {
    let userCached = this.store.freewillies.some((willy) => {
      if (willy.idUser === idUser) {
        //check conIds
        let conIdCached = willy.conIds.some((id) => {
          return conId === id;
        });
        // user found, only conid hat to be pushed
        if (!conIdCached) {
          willy.conIds.push(conId);
        }
        return true;
      }
      return false;
    });
    if (!userCached) {
      this.store.freewillies.push({
        idUser: idUser,
        apiToken: apiToken,
        conIds: [conId],
        username: username
      });
    }
  }

  removeUserFromFreeWilliesByApiToken(apiToken) {
    for (let i = 0; i < this.store.freewillies.length; i++) {
      if (this.store.freewillies[i].apiToken === apiToken) {
        return this.store.freewillies.splice(i, 1);
      }
    }
    console.log(`ERROR: Could not find Cached User with apiToken ${apiToken}`);
  }

  joinRoom(apiToken, idRoom) {
    let willy = this.removeUserFromFreeWilliesByApiToken(apiToken);
    if (willy) {
      if (!this.store.rooms.hasOwnProperty(idRoom)) {
        this.store.rooms[idRoom] = [];
      }
      this.store.rooms[idRoom].push(willy);
    }
  }

  leaveRoom(apiToken, idRoom) {
    let user = this.removeUserFromRoomByApiToken(apiToken, idRoom);
    this.addUserToFreeWilly(user.conIds, user.idUser, user.apiToken, user.username);
  }

  removeUserFromRoomByApiToken(apiToken, idRoom) {
    //TODO remove user by apiToken from room
    if (this.store.rooms.hasOwnProperty(idRoom)) {
      for (let i = 0; i < this.store.rooms[idRoom].length; i++) {
        if (this.store.rooms[idRoom].apiToken === apiToken) {
          return this.store.rooms[idRoom].splice(i, 1);
        }
      }
    }
    console.log(`ERROR: Could not find User with apiToken ${apiToken} in cached groups`);
  }


  static isEqual(value, other) {
    // Get the value type
    let type = Object.prototype.toString.call(value);
    // If the two objects are not the same type, return false
    if (type !== Object.prototype.toString.call(other)) return false;
    // If items are not an object or array, return false
    if (['[object Array]', '[object Object]'].indexOf(type) < 0) return false;
    // Compare the length of the length of the two items
    let valueLen = type === '[object Array]' ? value.length : Object.keys(value).length;
    let otherLen = type === '[object Array]' ? other.length : Object.keys(other).length;
    if (valueLen !== otherLen) return false;
    // Compare two items
    let compare = function (item1, item2) {
      // Get the object type
      let itemType = Object.prototype.toString.call(item1);
      // If an object or array, compare recursively
      if (['[object Array]', '[object Object]'].indexOf(itemType) >= 0) {
        if (!SimpleCache.isEqual(item1, item2)) return false;
      }
      // Otherwise, do a simple comparison
      else {
        // If the two items are not the same type, return false
        if (itemType !== Object.prototype.toString.call(item2)) return false;

        // Else if it's a function, convert to a string and compare
        // Otherwise, just compare
        if (itemType === '[object Function]') {
          if (item1.toString() !== item2.toString()) return false;
        } else {
          if (item1 !== item2) return false;
        }

      }
    };
    // Compare properties
    if (type === '[object Array]') {
      for (let i = 0; i < valueLen; i++) {
        if (compare(value[i], other[i]) === false) return false;
      }
    } else {
      for (let key in value) {
        if (value.hasOwnProperty(key)) {
          if (compare(value[key], other[key]) === false) return false;
        }
      }
    }
    // If nothing failed, return true
    return true;
  };
}

module.exports = new SimpleCache();