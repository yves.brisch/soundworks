const sqlite = require('sqlite3').verbose(),
  fs = require('fs'),
  Room = require('./models/Room'),
  Playlist = require('./models/Playlist'),
  ModRight = require('./models/ModRight'),
  Song = require('./models/Song'),
  Account = require('./models/Account'),
  AccountHasModRightForRoom = require('./models/AccountHasModRightForRoom'),
  AccountHasModRightForPlaylist = require('./models/AccountHasModRightForPlaylist');

/**
 * this class offers basic methods needed for the sqlite database interaction
 */
class Sqlite3Database {
  constructor() {
    this.db = null;
    this.fileName = global.appRoot + "/soundworks.db";
    this.isFirstRun = true;
  }

  /**
   * initialize and/or connect to database (https://www.npmjs.com/package/sqlite3)
   * default mode is OPEN_READWRITE | OPEN_CREATE
   */
  init() {
    return new Promise((resolve, reject) => {
      this._getConnection().then((db) => {
        this.db = db;
        if (this.isFirstRun) {
          this._createTables();
        }
        resolve();
      }).catch((err) => reject(err))
    });
  };

  /**
   * connect to local file based sqlite database
   * @private
   */
  _getConnection() {
    // if db file exists we do not have to create tables later on
    this.isFirstRun = !fs.existsSync(this.fileName);
    return new Promise((resolve, reject) => {

      const db = new sqlite.Database(this.fileName, (err) => {
        if (err) {
          console.error("ERROR:", "Could not establish database connection. Exitting process due to:", err.message);
          return reject(err);
        }
        console.log('INFO: Opened database connection');
        resolve(db);
      });
    });
  };

  /**
   * create all tables. Use queries of static model functions
   * @private
   */
  _createTables() {
    this.db.serialize(() => {
      console.log("Creating Tables.");
      //create tables - order is important due to constraints! - sqlite does not allow adding constraint after creation
      // no fks
      this.db.run(Playlist.createTableQuery(), this._logError());
      this.db.run(ModRight.createTableQuery(), this._logError());
      this.db.run(Account.createTableQuery(), this._logError());
      // 1 fk
      this.db.run(Room.createTableQuery(), this._logError());
      this.db.run(Song.createTableQuery(), this._logError());

      // 3 fks
      this.db.run(AccountHasModRightForRoom.createTableQuery(), this._logError());
      this.db.run(AccountHasModRightForPlaylist.createTableQuery(), this._logError());
    });
  }

  /**
   *
   * @param err
   * @private
   */
  _logError(err) {
    if (err) {
      console.log(`ERROR: ${err}`);
    }
  }

  /**
   * get the current date in database format for created_at and updated_at columns
   * @private
   */
  _getDate() {
    return Date.now();
  }

  /**
   *
   * @returns {string}
   * @private
   */
  static _generateApiToken() {
    return Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15);
  }

  // run db statements
  /**
   * run insert or create statements. Return promise with the given obj
   */
  _runWithPromiseAutoGenConstrId(query, insertedObj, propsToRemove) {
    return new Promise((resolve, reject) => {
      this.db.run(query, [], function (err) {
        if (err) {
          console.error("ERROR: Error while executing query: [%s] - %s", query, err.message);
          return reject(err);
        }
        if (insertedObj && insertedObj.constructor.name !== "Object") {
          let id = 'id' + insertedObj.constructor.name;
          // noinspection JSUnresolvedVariable
          insertedObj[id] = this.lastID;
          if (propsToRemove && Array.isArray(propsToRemove)) {
            propsToRemove.forEach((prop) => {
              if (insertedObj.hasOwnProperty(prop)) {
                delete insertedObj[prop];
              }
            });
          }
          resolve(insertedObj);
        } else {
          resolve({});
        }
      });
    });
  }

  /**
   *
   * @param query
   * @param propName
   * @param value
   * @returns {Promise<any>}
   * @private
   */
  _runWithPromiseCustomResponse(query, propName, value) {
    return new Promise((resolve, reject) => {
      this.db.run(query, [], function (err) {
        if (err) {
          console.error("ERROR: Error while executing query: [%s] - %s", query, err.message);
          return reject(err);
        }
        if (propName && value) {
          let res = {};
          res[propName] = value;
          resolve(res);
        } else {
          resolve({});
        }
      });
    });
  }

  /**
   *
   * @param query
   * @param propName
   * @param value
   * @returns {Promise<any>}
   * @private
   */
  _getWithPromise(query) {
    return new Promise((resolve, reject) => {
      this.db.get(query, [], function (err, row) {
        if (err) {
          console.error("ERROR: Error while executing query: [%s] - %s", query, err.message);
          return reject(err);
        }
        resolve(row);
      });
    });
  }

  /**
   * run select statements with several results. Return promise
   * @param query
   * @returns {Promise<any>}
   * @private
   */
  _eachWithPromise(query) {
    let data = [];
    return new Promise((resolve, reject) => {
      this.db.each(query, [], function (err, row) {
        if (err) {
          reject(err);
        }
        data.push(row);
      }, () => {
        resolve(data)
      });
    });
  }

  // db insert statements
  /**
   * create a new room with the given name
   * @param name
   * @param callback is called after the insert is done
   */
  addRoom(name) {
    let date = this._getDate(),
      room = new Room(name, null, null, date, date);
    const query = `INSERT INTO ${Room.tableName()} (id_playlist, name, created_at, updated_at) 
                   VALUES (null, '${room.getName()}', '${room.getCreatedAt()}', '${room.getUpdatedAt()}')`;
    return this._runWithPromiseAutoGenConstrId(query, room);
  }

  /**
   * create a new room Playlist the given name
   * @param name
   * @param callback is called after the insert is done
   */
  addPlaylist(name) {
    let date = this._getDate(),
      playlist = new Playlist(name, null, date, date);
    const query = `INSERT INTO ${Playlist.tableName()} (name, created_at, updated_at) 
                   VALUES ('${playlist.getName()}', '${playlist.getCreatedAt()}', '${playlist.getUpdatedAt()}')`;
    return this._runWithPromiseAutoGenConstrId(query, playlist);
  }

  /**
   *
   * @param username
   * @param password
   */
  addUser(username, password) {
    let date = this._getDate(),
      acc = new Account(username, null, null, password, Sqlite3Database._generateApiToken(), date, date);
    const query = `INSERT INTO ${Account.tableName()} (name, email, password, api_token, created_at, updated_at) 
                   VALUES ('${acc.name}', '${acc.email}', '${acc.password}', '${acc.apiToken}', '${acc.createdAt}', '${acc.updatedAt}')`;
    return this._runWithPromiseAutoGenConstrId(query, acc, ["password", "email", "createdAt", "updatedAt"]);
  }

  /**
   *
   * @param idPlaylist
   * @param name
   * @param url
   */
  addSong(idPlaylist, name, url) {
    let date = this._getDate(),
      song = new Song(null, idPlaylist, url, name, 0, 0, 0, null, date, date);
    const query = `INSERT INTO ${Song.tableName()} 
                   (id_playlist, url, title, length, upvotes, downvotes, id_prev_song, created_at, updated_at)
                   SELECT '${idPlaylist}', '${url}', '${name}', '${song.length}', '${song.upvotes}', '${song.downvotes}',
                   MAX(id_song), '${song.createdAt}', '${song.updatedAt}' 
                   FROM ${Song.tableName()} WHERE id_playlist = '${idPlaylist}'`;
    return this._runWithPromiseAutoGenConstrId(query, song, ["createdAt", "updatedAt"]);
  }

  // db select statements
  /**
   *
   * @param username
   * @param password
   * @returns {Promise<*>}
   */
  async getUser(username, password) {
    let query = `SELECT * FROM ${Account.tableName()} WHERE name = '${username}' AND password = '${password}'`;
    let row = await this._getWithPromise(query);
    // row is undefined if not found
    if (!row) {
      return null;
    } else {
      return {idUser: row.id_account, apiToken: row.api_token, username: row.name};
    }
  }

  /**
   *
   * @param apiToken
   * @returns {Promise<*>}
   */
  async validateUser(apiToken) {
    let query = `SELECT * FROM ${Account.tableName()} WHERE api_token = '${apiToken}'`;
    let row = await this._getWithPromise(query);
    // row is undefined if not found
    console.log("retrieved data");
    return !!row;
  }

  /**
   * return all rooms as JSON
   */
  async getRooms() {
    let query = "SELECT * FROM room";
    let res = [];
    await this._eachWithPromise(query).then((data) => {
      data.forEach((item) => {
        let room = new Room(item.name, item.id_room, item.id_playlist, item.created_at, item.updated_at);
        res.push(room);
      });
    }).catch((err) => res = err);
    return res;
  }

  /**
   * return all playlists as JSON
   */
  async getPlaylists() {
    let query = `SELECT * FROM ${Playlist.tableName()}`;
    let res = [];
    await this._eachWithPromise(query).then((data) => {
      data.forEach((item) => {
        let playlist = new Playlist(item.name, item.id_playlist, item.created_at, item.updated_at);
        res.push(playlist);
      });
    }).catch((err) => res = err);
    return res;
  }

  // db delete statements
  /**
   * delete room with the given id
   * @param id
   */
  deleteRoom(id) {
    const query = `DELETE FROM ${Room.tableName()} WHERE id_room = ${id}`;
    return this._runWithPromiseCustomResponse(query, "idRoom", id);
  }

  /**
   * delete playlist with the given id
   * @param id
   */
  deletePlaylist(id) {
    const query = `DELETE FROM ${Playlist.tableName()} WHERE id_playlist = ${id}`;
    return this._runWithPromiseCustomResponse(query, "idPlaylist", id);
  }

  // db update statements
  /**
   *
   * @param idRoom
   * @param idPlaylist
   */
  addPlaylistToRoom(idRoom, idPlaylist) {
    const query = `UPDATE ${Room.tableName()} SET id_playlist = ${idPlaylist} WHERE id_room = ${idRoom}`;
    return this._runWithPromiseAutoGenConstrId(query, {idRoom: idRoom, idPlaylist: idPlaylist});
  }
}

module.exports = Sqlite3Database;