/**
 * data as object
 */
class JSONResponse {
  constructor(success, error, action , data = {}) {
    return {
      success: success,
      error: error,
      action: action,
      data: data
    };
  }
}
module.exports = JSONResponse;