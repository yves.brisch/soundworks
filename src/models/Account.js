class Account {
  constructor(name, idAccount, email, password, apiToken, createdAt, updatedAt) {
    this.name = name;
    this.idAccount = idAccount;
    this.email = email;
    this.password = password;
    this.apiToken = apiToken;
    this.createdAt = createdAt;
    this.updatedAt = updatedAt;
  }

  static tableName() {
    return "account";
  }
  /**
   * return create table query as string
   */
  static createTableQuery() {
    return `CREATE TABLE ${this.tableName()} ( 
        id_account integer PRIMARY KEY, 
        name text NOT NULL,
        email text,
        password text NOT NULL,
        api_token text,
        created_at text NOT NULL, 
        updated_at text NOT NULL 
      )`;
  }
}

module.exports = Account;