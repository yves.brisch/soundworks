class AccountHasModRightForRoom {
  constructor() {
  }

  static tableName() {
    return "account_has_mod_right_for_room";
  }
  /**
   * return create table query as string
   */
  static createTableQuery() {
    return `CREATE TABLE ${this.tableName()} ( 
        id_account integer, 
        id_room integer,
        id_mod_right integer,
        FOREIGN KEY (id_account) REFERENCES account (id_account) 
        ON DELETE CASCADE ON UPDATE CASCADE,
        FOREIGN KEY (id_room) REFERENCES room (id_room) 
        ON DELETE CASCADE ON UPDATE CASCADE,
        FOREIGN KEY (id_mod_right) REFERENCES mod_right (id_mod_right) 
        ON DELETE CASCADE ON UPDATE CASCADE
      )`;
  }
}

module.exports = AccountHasModRightForRoom;