class Room {
  constructor(name, idRoom, idPlaylist, createdAt, updatedAt) {
    this.name = name;
    this.idRoom = idRoom;
    this.idPlaylist = idPlaylist;
    this.createdAt = createdAt;
    this.updatedAt = updatedAt;
  }

  getName() {
    return this.name;
  }

  setName(name) {
    this.name = name | function () {
      console.log("ERROR:", "No name for room given.");
      this.name = "";
    };
  }

  getIdRoom() {
    return this.idRoom;
  }

  getIdPlaylist() {
    return this.idPlaylist;
  }

  getCreatedAt() {
    return this.createdAt;
  }

  getUpdatedAt() {
    return this.updatedAt;
  }

  static tableName() {
    return "room";
  }

  /**
   * return create table query as string
   */
  static createTableQuery() {
    return `CREATE TABLE ${this.tableName()} ( 
        id_room integer PRIMARY KEY, 
        id_playlist integer, 
        name text NOT NULL,
        created_at text NOT NULL, 
        updated_at text NOT NULL,
        FOREIGN KEY (id_playlist) REFERENCES playlist (id_playlist) 
        ON DELETE RESTRICT ON UPDATE NO ACTION
      )`;
  }
}

module.exports = Room;