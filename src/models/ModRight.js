class ModRight {
  constructor(name) {
    this.idModRight = -1;
    this.description = name;
  }

  static tableName() {
    return "mod_right";
  }

  /**
   * return create table query as string
   */
  static createTableQuery() {
    return `CREATE TABLE ${this.tableName()} ( 
        id_mod_right integer PRIMARY KEY, 
        description text NOT NULL
      )`;
  }
}

module.exports = ModRight;