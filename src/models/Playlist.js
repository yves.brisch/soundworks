class Playlist {
  constructor(name, idPlaylist, createdAt, updated_at) {
    this.name = name;
    this.idPlaylist = idPlaylist;
    this.createdAt = createdAt;
    this.updatedAt = updated_at;
  }

  getName() {
    return this.name;
  }

  getIdPlaylist() {
    return this.idPlaylist;
  }

  getCreatedAt() {
    return this.createdAt;
  }

  getUpdatedAt() {
    return this.updatedAt;
  }

  static tableName() {
    return "playlist";
  }

  /**
   * return create table query as string
   */
  static createTableQuery() {
    return `CREATE TABLE ${this.tableName()} ( 
        id_playlist integer PRIMARY KEY, 
        name text NOT NULL,
        created_at text NOT NULL, 
        updated_at text NOT NULL 
      )`;
  }
}

module.exports = Playlist;