class Song {
  constructor(idSong, idPlaylist, url, title, length, upvotes, downvotes, idPrevSong, createdAt, updatedAt) {
    this.idSong = -1;
    this.idPlaylist = -1;
    this.url = url;
    this.title = title;
    this.length = length;
    this.upvotes = upvotes;
    this.downvotes = downvotes;
    this.idPrevSong = idPrevSong;
    this.createdAt = createdAt;
    this.updatedAt = updatedAt;
  }

  static tableName() {
    return "song";
  }
  /**
   * return create table query as string
   */
  static createTableQuery() {
    return `CREATE TABLE ${this.tableName()} ( 
        id_song integer PRIMARY KEY, 
        id_playlist integer, 
        url text NOT NULL,
        title text,
        length real,
        upvotes integer,
        downvotes integer,
        id_prev_song integer,
        created_at text NOT NULL, 
        updated_at text NOT NULL,
        FOREIGN KEY (id_prev_song) REFERENCES song (id_song),
        FOREIGN KEY (id_playlist) REFERENCES playlist (id_playlist) 
        ON DELETE NO ACTION ON UPDATE NO ACTION
      )`;
  }
}

module.exports = Song;