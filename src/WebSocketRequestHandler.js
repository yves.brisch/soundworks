const {MissingPropertyError, NotAuthorizedError, BreakError} = require('./Errors'),
  JSONResponse = require('./JSONResponse'),
  Sqlite3Database = require('./Sqlite3Database'),
  Cache = require('../src/SimpleCache');

class WebSocketRequestHandler {
  constructor() {
    this.db = new Sqlite3Database();
    this.db.init();
  }

  //constants
  static get SEND() {
    return 0;
  }

  static get BROADCAST_ALL() {
    return 1;
  }

  static get BROADCAST_ROOM() {
    return 2;
  }

  /**
   * handle incoming messages, check for validity, pass on to {@link _handleAction} if the message is a valid json
   * @param message the message to validate and then handle
   * @param conId
   * @returns a {@code JSON} which always contains a {@code boolean success} attr and a {@code string error} attr
   */
  async handle(message, conId) {
    let json, data;
    try {
      // check if message is a valid JSON
      json = JSON.parse(message);
      console.log(`DEBUG: incoming request: ${JSON.stringify(json)}`);
      // validate if user is logged in
      if (!await this._isUserValid(json)) {
        return new JSONResponse(false, "User is not logged in.", "invalidUser", {});
      }
      // handle actions
      data = await this._handleAction(json, conId);
    } catch (e) {
      if (e instanceof MissingPropertyError) {
        console.log("ERROR: %s", e.message);
        console.log("DEBUG: %s", message);
      } else if (e instanceof NotAuthorizedError) {
        console.log("ERROR: %s", e.message);
        console.log("DEBUG: %s", message);
      } else {
        console.log(`ERROR: message is not a valid JSON: ${message}`);
        console.log(e);
      }
      return new JSONResponse(false, "message is not a valid JSON", null, {});
    }
    return data;
  }

  /**
   *
   * @param json
   * @returns {boolean}
   * @private
   */
  async _isUserValid(json) {
    // check if apiToken is in db
    if (!json.hasOwnProperty("action")) {
      throw new MissingPropertyError("No action property found for this request,");
    }
    if (json.action === "login" || json.action === "register") {
      return true;
    } else if (json.hasOwnProperty("apiToken")) {
      // validate is false if no user was found
      return await this.db.validateUser(json.apiToken);
    }
    return false;
  }

  /**
   * this method checks whether the request contains a {@code string action} attr and thus parses it
   * @param json
   * @private
   */
  async _handleAction(json, conId) {
    if (!conId) {
      throw new MissingPropertyError("No conId found for this websocket. This is a server error.")
    }
    let action = json.action,
      token = json.apiToken,
      data = {},
      username, password, name, id;
    switch (action) {
      case 'login':
        username = json.username;
        password = json.password;
        if (!username || !password) {
          let msg = `missing parameter 'username' and/or 'password' for action ${action}`;
          return new JSONResponse(false, msg, action, {});
        }
        data = await this.db.getUser(username, password);
        if (!data) {
          return new JSONResponse(false, "Incorrect username or password", action, {});
        }
        // register user in cache - without room
        Cache.addUserToFreeWilly(conId, data.idUser, data.apiToken, data.username);
        data.answermode = WebSocketRequestHandler.SEND;
        break;
      case 'register':
        username = json.username;
        password = json.password;
        if (!username || !password) {
          let msg = `missing parameter 'username' and/or 'password' for action ${action}`;
          return new JSONResponse(false, msg, action, {});
        }
        data = await this.db.addUser(username, password);
        // register user in cache - without room
        Cache.addUserToFreeWilly(conId, data.idAccount, data.apiToken, data.username);
        data.answermode = WebSocketRequestHandler.SEND;
        break;
      case 'getPlaylists':
        data = await this.db.getPlaylists();
        data.answermode = WebSocketRequestHandler.SEND;
        break;
      case 'addPlaylist':
        name = json.name;
        if (!name) {
          let msg = `missing parameter 'name' for action ${action}`;
          return new JSONResponse(false, msg, action, {});
        }
        data = await this.db.addPlaylist(name);
        data.answermode = WebSocketRequestHandler.BROADCAST_ALL;
        break;
      case 'deletePlaylist':
        id = json.idPlaylist;
        if (!id) {
          let msg = `missing parameter 'id' for action ${action}`;
          return new JSONResponse(false, msg, action, {});
        }
        data = await this.db.deletePlaylist(id);
        data.answermode = WebSocketRequestHandler.BROADCAST_ALL;
        break;
      case 'getRooms':
        data = await this.db.getRooms();
        data.answermode = WebSocketRequestHandler.SEND;
        break;
      case 'addRoom':
        name = json.name;
        if (!name) {
          let msg = `missing parameter 'name' for action ${action}`;
          return new JSONResponse(false, msg, action, {});
        }
        data = await this.db.addRoom(name);
        data.answermode = WebSocketRequestHandler.BROADCAST_ALL;
        break;
      case 'deleteRoom':
        id = json.idRoom;
        if (!id) {
          let msg = `missing parameter 'id' for action ${action}`;
          return new JSONResponse(false, msg, action, {});
        }
        data = await this.db.deleteRoom(id);
        data.answermode = WebSocketRequestHandler.BROADCAST_ALL;
        break;
      case 'addPlaylistToRoom':
        let idRoom = json.idRoom;
        let idPlaylist = json.idPlaylist;
        if (!idRoom || !idPlaylist) {
          let msg = `missing parameter 'idRoom' and/or 'idPlaylist' for action ${action}`;
          return new JSONResponse(false, msg, action, {});
        }
        data = await this.db.addPlaylistToRoom(idRoom, idPlaylist);
        data.answermode = WebSocketRequestHandler.BROADCAST_ALL;
        break;
      case 'addSong':
        //name, url, idPlaylist
        id = json.idPlaylist;
        name = json.name;
        let url = json.url;
        if (!id || !name || !url) {
          let msg = `missing parameter 'idPlaylist' and/or 'name' and/or 'url' for action ${action}`;
          return new JSONResponse(false, msg, action, {});
        }
        data = await this.db.addSong(id, name, url);
        data.answermode = WebSocketRequestHandler.BROADCAST_ALL;
        break;
      case 'joinRoom':
        id = json.idRoom;
        if (!id) {
          let msg = `missing parameter 'idRoom' for action ${action}`;
          return new JSONResponse(false, msg, action, {});
        }
        Cache.joinRoom(token, id);
        //TODO fill usernames, get username
        return [new JSONResponse(true, null, action, {answermode: WebSocketRequestHandler.SEND, idRoom: id, usernames:[]}),
          new JSONResponse(true, null, action, {answermode: WebSocketRequestHandler.BROADCAST_ROOM, idRoom: id, username: ""})];
      case 'leaveRoom':
        id = json.idRoom;
        if (!id) {
          let msg = `missing parameter 'idRoom' for action ${action}`;
          return new JSONResponse(false, msg, action, {});
        }
        Cache.leaveRoom(token, id);
        return [new JSONResponse(true, null, action, {answermode: WebSocketRequestHandler.SEND, idRoom: id}),
          new JSONResponse(true, null, action, {answermode: WebSocketRequestHandler.BROADCAST_ROOM, idRoom: id})];
      case 'getPlaylist':
        //TODO
      default:
        let msg = `action '${action}' is not supported.`;
        return new JSONResponse(false, msg, action, {});
    }
    return new JSONResponse(true, null, action, data);
  };
}

module.exports = WebSocketRequestHandler;