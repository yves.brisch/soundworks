const express = require('express'),
  path = require('path'),
  app = express(),
  Sqlite3Database = require('./src/Sqlite3Database');

global.appRoot = path.resolve(__dirname);


//init sqlite3 database
const database = new Sqlite3Database();
database.init();


app.use(express.json());
app.use(express.urlencoded({extended: false}));

// handle 404
app.use(function (req, res) {
  if (!res.headersSent) {
    res.status(404).send("404 TODO"); // TODO 404 page or redirect
  }
});

// handle all errors
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.send(res.status() + " TODO"); // TODO 500 page or redirect
});

module.exports = app;
