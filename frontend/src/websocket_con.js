import { store, mutations } from './store'
import {router} from './main';

class WS {
    static ws;
    static connect() {
        return new Promise((resolve) => {
            this.ws = new WebSocket('ws://localhost:3000');
            this.ws.onopen = () => {
                this.ws.addEventListener("message", (event) => {
                    let rsp = JSON.parse(event.data);
                    if (rsp.action !== undefined) {
                        //console.log(rsp);
                        switch (rsp.action) {
                            case "login": {
                                if (rsp.success === true) {
                                    router.push('dashboard');
                                    store.logged_in = true;
                                    localStorage.token = rsp.data.apiToken
                                }
                                break;
                            }
                            case "invalidUser":
                                router.push('/');
                                store.logged_in = false;
                                break;
                            case "register":
                                localStorage.token = rsp.data.apiToken;
                                break;
                            case "getRooms":
                                store.rooms = rsp.data;
                                break;
                            case "addRoom": {
                                store.rooms.push(rsp.data);
                                break;
                            }
                            case "deleteRoom": {
                                let index = 0;
                                store.rooms.forEach(function(element){
                                   if(element.idRoom === rsp.data.idRoom){
                                       store.rooms.splice(index, 1)
                                   }
                                   index++;
                                });
                                //self.rooms.push(rsp.data);
                                break;
                            }
                            case "deletePlaylist": {
                                let index = 0;
                                store.playlists.forEach(function(element){
                                    if(element.idPlaylist === rsp.data.idPlaylist){
                                        store.playlists.splice(index, 1)
                                    }
                                    index++;
                                });
                                //self.rooms.push(rsp.data);
                                break;
                            }
                            case "getPlaylists":
                                store.playlists = rsp.data;
                                break;
                            case "getPlaylist":
                                //store.playlist = rsp.data;
                                break;
                            case "addPlaylist":
                                store.playlists.push(rsp.data);
                                break;
                            case "joinRoom":
                                store.current_room = rsp.data.idRoom;
                            break;
                            case "addSong":
                                store.playlist.push(rsp.data)
                            break;
                        }
                    }
                });
                resolve()
            };
        });
    }

    static disconnect() {
        this.ws.onclose = function () {
        };
        this.ws.close();
    }

    static send(msg){
        let self = this;
        if (this.ws.readyState !== 1) {
            this.ws.onopen = function () {
                self.send(msg);
            };
        } else {
            this.ws.send(msg);
        }
    }

    static getWS() {
            return this.ws;
    }

    static WSEventListener() {


    }
}
export default WS