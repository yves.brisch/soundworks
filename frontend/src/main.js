import Vue from 'vue'
import App from './App.vue'
import PlayerWrapper from './components/player/player-wrapper'
import VueRouter from 'vue-router'
import Dashboard from './components/dashboard/dashboard'
import LoginRegister from './components/login_register/login_register'
import Playlist from './components/playlist/playlist'
import { library } from '@fortawesome/fontawesome-svg-core'
import { faCoffee, faEllipsisV, faChevronCircleUp, faChevronCircleDown, faTrash } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import VueYoutube from 'vue-youtube'
import { store, mutations } from './store'

library.add(faCoffee, faEllipsisV, faChevronCircleUp, faChevronCircleDown, faTrash)
Vue.component('font-awesome-icon', FontAwesomeIcon)

Vue.config.productionTip = false
Vue.use(VueRouter)
Vue.use(VueYoutube)

const routes = [
  { path: '/player', component: PlayerWrapper},
  { path: '/playlist/:id_playlist', name: 'playlist', component: Playlist, props: true},
  { path: '/dashboard', component: Dashboard, meta: { requiresAuth: true } },
  { path: '/', component: LoginRegister },
]

const router = new VueRouter({
  routes // short for `routes: routes`
})



new Vue({
  router,
  render: h => h(App),
}).$mount('#app')

export {router}