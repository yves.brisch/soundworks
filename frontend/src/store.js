
import Vue from 'vue';

export const store = Vue.observable({
    rooms: [],
    playlists: [],
    current_room: null,
    playlist: [],
    logged_in: false,
});

export const mutations = {

}